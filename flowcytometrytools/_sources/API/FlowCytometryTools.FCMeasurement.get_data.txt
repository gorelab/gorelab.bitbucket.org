FlowCytometryTools.FCMeasurement.get_data
=========================================

.. currentmodule:: FlowCytometryTools

.. automethod:: FCMeasurement.get_data