FlowCytometryTools.PolyGate
===========================

.. currentmodule:: FlowCytometryTools

.. autoclass:: PolyGate
    :members:
