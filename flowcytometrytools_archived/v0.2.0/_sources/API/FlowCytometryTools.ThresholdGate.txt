FlowCytometryTools.ThresholdGate
================================

.. currentmodule:: FlowCytometryTools

.. autoclass:: ThresholdGate
    :members:
