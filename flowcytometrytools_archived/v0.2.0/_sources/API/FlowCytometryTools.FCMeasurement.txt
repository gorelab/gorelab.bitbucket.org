FlowCytometryTools.FCMeasurement
================================

.. currentmodule:: FlowCytometryTools

.. autoclass:: FCMeasurement
    :members:
