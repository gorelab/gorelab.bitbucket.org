Gallery
-------------------------------

Single FCS Files
===================

1d histograms

.. plot:: pyplots/sample_1dhist_plot.py


2d histograms

.. plot:: pyplots/sample_2dhist_plot.py


96-well Plates
===================

1d histograms

.. plot:: pyplots/plate_1dhist_plot.py
