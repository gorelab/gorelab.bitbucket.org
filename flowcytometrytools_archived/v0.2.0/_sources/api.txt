.. _api:

.. currentmodule:: FlowCytometryTools

************
API
************

Containers
----------------------------

.. autosummary::
    :toctree: API
    :template: simple.rst

    FCMeasurement
    FCOrderedCollection

Gating
----------------------------

.. autosummary::
    :toctree: API
    :template: simple.rst

    ThresholdGate
    IntervalGate
    QuadGate
    PolyGate 
